<p align="right"><a href="README-de.md">Deutsch</a> &nbsp; <a href="README.md">English</a></p>

# Wittstock 0.8.18

Wittstock is a classless theme for Datenstrom Yellow.

<p align="center"><img src="wittstock-screenshot.png?raw=true" width="795" height="836" alt="Screenshot"></p>

## How to customise a theme

All theme files are stored in your `system/themes` folder. All layout files are stored in your `system/layouts` folder. You can edit these files. Your changes will not be overwritten when the website is updated.

## Known issues

Some Yellow-related CSS definitions are currently not included (e. g. warnings and notices). You can add them in `wittstock-custom.css` if you need these classes. 

## Installation

[Download extension](https://github.com/datenstrom/yellow-extensions/raw/master/zip/wittstock.zip) and copy zip file into your `system/extensions` folder. Right click if you use Safari.

This extension uses [Simple.css 2.1.0](https://github.com/kevquirk/simple.css) by Kev Quirk. 

## Designer

Steffen Schultz. [Get help](https://github.com/schulle4u/yellow-extensions-schulle4u/issues).

