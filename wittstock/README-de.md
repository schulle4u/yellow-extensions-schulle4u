<p align="right"><a href="README-de.md">Deutsch</a> &nbsp; <a href="README.md">English</a></p>

# Wittstock 0.8.18

Wittstock ist ein klassenloses Theme für Datenstrom Yellow.

<p align="center"><img src="wittstock-screenshot.png?raw=true" width="795" height="836" alt="Bildschirmfoto"></p>

## Wie man ein Thema anpasst

Alle Themendateien befinden sich im `system/themes`-Verzeichnis. Alle Layoutdateien befinden sich im `system/layouts`-Verzeichnis. Du kannst diese Dateien bearbeiten. Deine Änderungen werden bei der Aktualisierung der Webseite nicht überschrieben.

## Bekannte Probleme

Einige der von Yellow verwendeten CSS-Definitionen sind derzeit nicht enthalten (z. B. Warnungen und Hinweise). Sie können jedoch in der Datei `wittstock-custom.css` hinterlegt werden, solltest du sie benötigen. 

## Installation

[Erweiterung herunterladen](https://github.com/datenstrom/yellow-extensions/raw/master/zip/wittstock.zip) und die Zip-Datei in dein `system/extensions`-Verzeichnis kopieren. Rechtsklick bei Safari.

Diese Erweiterung benutzt [Simple.css 2.1.0](https://github.com/kevquirk/simple.css) von Kev Quirk. 

## Designer

Steffen Schultz. [Hilfe finden](https://github.com/schulle4u/yellow-extensions-schulle4u/issues).
